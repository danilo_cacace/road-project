#!/bin/bash

environment=$1
app_name=$2
IFS=',' read -r -a countries <<< "$3"

kubectl config use-context $environment-cluster


for country in "${countries[@]}"
do
    helm delete $app_name-$country -n $country
    kubectl delete secret secret-envs -n $country
done