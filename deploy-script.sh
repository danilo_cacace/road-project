#!/bin/bash

## Requirements:
##   - minikube
##   - helm

# Args
environment=$1
app_name=$2
IFS=',' read -r -a countries <<< "$3"

# Switch to the right cluster
kubectl config use-context $environment-cluster

# Create the secret to recover the env variables and
# install the chart (same for app1 and app2) with the proper values
for country in "${countries[@]}"
do
    kubectl create namespace $country 2>/dev/null
    kubectl create secret generic secret-envs --from-env-file=./samples/${app_name}/deploy/${environment}.env -n $country
    helm install $app_name-$country -n $country --set applicationName=$app_name --set country=$country ./road-chart

    echo "Succesfully deployed $app_name on $environment for $countries"
    echo
done

echo "Application installed, add at the bottom of the '/ect/hosts' file the following lines"

for country in "${countries[@]}"
do
    echo $(minikube ip -p $environment-cluster) $(kubectl get ingress -n $country | grep -o '\w*-\w*.info' )

done


