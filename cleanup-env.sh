#!/bin/bash
minikube delete -p staging-cluster 
minikube delete -p production-cluster
echo "Stopped" $(docker stop registry)
echo "Deleted" $(docker rm registry)