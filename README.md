## Requirements

- Install [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- Install [Helm](https://helm.sh/docs/intro/install/)
- Clone this repo locally and locate a terminal in root `road-project` directory.

## Local Environment Set Up

- Run the `start-env`  script to prepare your local environment and save the Grafana default credentials reported at the end

```bash
./start-env.sh
```

> N.B. You can access to Prometheus and Grafana by port-forward
> 

## Deploy Application

Run the `deploy-script` to deploy you favourite app in your favourite cluster 🙂

```bash
./deploy-script.sh <ENVIRONMENT> <APP_NAME> <COUNTRY>,<COUNTRY>,...,<COUNTRY>
```

E.g. `./deploy-script staging app1 it,es,uk`

## Cleanup

To cleanup the application deployed you can run the `cleanup-script` 

```bash
./cleanup-script.sh <ENVIRONMENT> <APP_NAME> <COUNTRY>,<COUNTRY>,...,<COUNTRY>
```

Instead to cleanup the local environment (delete all allocated resources) you can run the cleanup-env cript

```bash
./cleanup-env.sh
```