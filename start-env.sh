#!/bin/bash
## Requirements:
##   - minikube
##   - helm
##   - kubctl

# Start the clusters and enabling the NGINX Ingress
minikube start --nodes 1 -p staging-cluster
minikube addons enable ingress -p staging-cluster

minikube start --nodes 2 -p production-cluster
minikube addons enable ingress -p production-cluster

# Install Prometheus for metrics collection on production cluster
kubectl config use-context production-cluster
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus

# Expose Prometheus Service
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np

# Install Grafana for analytics and visualization
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana

# Expose Grafana Service
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np

# Run local docker registry
docker run -d -p 5000:5000 --restart=always --name registry registry:2


declare -a apps=("app1" "app2")

# Build the docker images and push them on registry
for app in "${apps[@]}"
do  
    docker build -t localhost:5000/$app ./samples/$app
    echo $app "image builded"
    docker push localhost:5000/$app
    echo $app "image pushed in registry"
done

echo
echo "Grafana default login credentials:"
echo "user: admin"
echo "pswd:" $(kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo)